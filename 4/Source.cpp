#include <locale.h>
#include <iostream>
#include <ObjBase.h>
using namespace std;
void AutoPlay(IDispatch* pXlApp, OLECHAR* name, WORD flag, VARIANT* params, int size, VARIANT* result) {
	DISPID dispID, dispidNamed = DISPID_PROPERTYPUT;
	DISPPARAMS dp = { NULL, NULL, 0, 0 };
	HRESULT hr;
	dp.cArgs = size;
	dp.rgvarg = params;
	if (flag & DISPATCH_PROPERTYPUT) {
		dp.cNamedArgs = size;
		dp.rgdispidNamedArgs = &dispidNamed;
	}
	hr = pXlApp->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispID);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Error :"<< name << ", getIDsOfNames HR = " << hr << endl;
		system("pause");
		_exit(hr);
	}
	hr = pXlApp->Invoke(dispID, IID_NULL, LOCALE_USER_DEFAULT, flag, (DISPPARAMS*)(&(*(&(*(&(*(&dp))))))), result, NULL, NULL);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Error :" << name << ", Invoke HR = " << endl;
		system("pause");
		_exit(hr);
	}
}
int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "rus");
	cout << "Start COM" << endl;
	if (FAILED(CoInitialize(NULL)))
	{
		cerr << "Bad start COM." << endl;
		system("pause");
		return 1;
	}
	WCHAR szwPPProgID[] = L"Excel.Application";
	CLSID clsid;
	HRESULT hr = ::CLSIDFromProgID(szwPPProgID, &clsid);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Error get CLSID for Excel.Application. HR = " << hr << endl;
		system("pause");
		return hr;
	}
	IDispatch* pXlApp = NULL;
	hr = CoCreateInstance(clsid, NULL, CLSCTX_LOCAL_SERVER, IID_IDispatch, (void**)&pXlApp);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Error get IDispatch for object Application. HR = " << hr << endl;
		system("pause");
		return hr;
	}
	// set visible window
	{
		VARIANT params[1];
		params[0].vt = VT_I4;
		params[0].lVal = 1;
		AutoPlay(pXlApp, L"Visible", DISPATCH_PROPERTYPUT, params, 1, NULL);
	}
	// get Workbooks collections
	IDispatch *pXlBooks;
	{
		VARIANT result;
		AutoPlay(pXlApp, L"Workbooks", DISPATCH_PROPERTYGET, NULL, 0, &result);
		pXlBooks = result.pdispVal;
	}
	// add Workbook in Workbooks collections
	IDispatch *pXlBook;
	{
		VARIANT result;
		AutoPlay(pXlBooks, L"Add", DISPATCH_PROPERTYGET, NULL, 0, &result);
		pXlBook = result.pdispVal;
	}
	// get active Workbook in Workbooks collections
	IDispatch *pXlSheet;
	{
		VARIANT result;
		AutoPlay(pXlApp, L"ActiveSheet", DISPATCH_PROPERTYGET, NULL, 0, &result);
		pXlSheet = result.pdispVal;
	}
	// get A1
	IDispatch *pXlRangeA1;
	{
		VARIANT result, params;
		params.vt = VT_BSTR;
		params.bstrVal = ::SysAllocString(L"A1");
		AutoPlay(pXlSheet, L"Range", DISPATCH_PROPERTYGET, &params, 1, &result);
		pXlRangeA1 = result.pdispVal;
	}
	// get A2
	IDispatch *pXlRangeA2;
	{
		VARIANT result, params;
		params.vt = VT_BSTR;
		params.bstrVal = ::SysAllocString(L"A2");
		AutoPlay(pXlSheet, L"Range", DISPATCH_PROPERTYGET, &params, 1, &result);
		pXlRangeA2 = result.pdispVal;
	}	
	// get A3
	IDispatch *pXlRangeA3;
	{
		VARIANT result, params;
		params.vt = VT_BSTR;
		params.bstrVal = ::SysAllocString(L"A3");
		AutoPlay(pXlSheet, L"Range", DISPATCH_PROPERTYGET, &params, 1, &result);
		pXlRangeA3 = result.pdispVal;
	}
	// set value 40 for A1
	{
		VARIANT params;
		params.vt = VT_BSTR;
		params.bstrVal = ::SysAllocString(L"40");
		AutoPlay(pXlRangeA1, L"Value", DISPATCH_PROPERTYPUT, &params, 1, NULL);
	}
	// set value 2 for A2
	{
		VARIANT params;
		params.vt = VT_BSTR;
		params.bstrVal = ::SysAllocString(L"2");
		AutoPlay(pXlRangeA2, L"Value", DISPATCH_PROPERTYPUT, &params, 1, NULL);
	}
	// set formula =A1+A2 for A3
	{
		VARIANT params;
		params.vt = VT_BSTR;
		params.bstrVal = ::SysAllocString(L"=A1+A2");
		AutoPlay(pXlRangeA3, L"Formula", DISPATCH_PROPERTYPUT, &params, 1, NULL);
	}
	// call method save as for project
	{
		VARIANT params[3];
		params[0].vt = VT_ERROR;
		params[0].scode = DISP_E_PARAMNOTFOUND;
		params[1].vt = VT_I4;
		params[1].lVal = 39;
		params[2].vt = VT_BSTR;
		params[2].bstrVal = ::SysAllocString(OLESTR("D:\\Excel.xls"));
		AutoPlay(pXlBook, L"SaveAs", DISPATCH_METHOD, params, 3, NULL);
	}
	// close window
	{
		AutoPlay(pXlApp, L"Quit", DISPATCH_METHOD, NULL, 0, NULL);
	}
	// clear memory for IDispatcher's
	pXlRangeA3->Release();
	pXlRangeA2->Release();
	pXlRangeA1->Release();
	pXlSheet->Release();
	pXlBook->Release();
	pXlBooks->Release();
	pXlApp->Release();

	CoUninitialize();
	cout << "End COM" << endl;
	system("pause");
	return 0;
}
