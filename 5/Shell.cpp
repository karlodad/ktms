#include <windows.h>
#include <shlobj.h>
#include "ClassFactory.h"
#include <olectl.h>

#pragma data_seg(".text")
#define INITGUID
#include <initguid.h>
#include <shlguid.h>
#include "Guid.h"
#pragma data_seg()
// data
HINSTANCE   g_hInst;
UINT        g_DllRefCount;
/*---------------------------------------------------------------*/
// DllMain()
/*---------------------------------------------------------------*/
BOOL APIENTRY DllMain(HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			g_hInst = (HINSTANCE)hModule;
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
	}
	return TRUE;
}
/*---------------------------------------------------------------*/
// DllCanUnloadNow()
/*---------------------------------------------------------------*/
STDAPI DllCanUnloadNow(VOID)
{
	return (g_DllRefCount ? S_FALSE : S_OK);
}
/*---------------------------------------------------------------*/
// DllGetClassObject()
/*---------------------------------------------------------------*/
STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID *ppReturn)
{
	*ppReturn = NULL;
	// Check CLSID 
	if (!IsEqualCLSID(rclsid, CLSID_SHELL))
		return CLASS_E_CLASSNOTAVAILABLE;
	// call the factory
	CClassFactory *pClassFactory = new CClassFactory();
	if (pClassFactory == NULL)
		return E_OUTOFMEMORY;
	// query interface for the a pointer
	HRESULT hResult = pClassFactory->QueryInterface(riid, ppReturn);
	pClassFactory->Release();
	return hResult;
}
typedef struct {
	HKEY  hRootKey;
	LPTSTR lpszSubKey;
	LPTSTR lpszValueName;
	LPTSTR lpszData;
}REGSTRUCT, *LPREGSTRUCT;

/*---------------------------------------------------------------*/
// DllRegisterServer()
/*---------------------------------------------------------------*/
STDAPI DllRegisterServer(VOID)
{
	size_t i;
	HKEY hKey;
	LRESULT lResult;
	DWORD dwDisp;
	LPWSTR pwsz;
	StringFromIID(CLSID_SHELL, &pwsz);
	TCHAR szSubKey[MAX_PATH], szModule[MAX_PATH], *szCLSID = new TCHAR[wcslen(pwsz) + 1];
	for (i = 0; i < wcslen(pwsz); i++)	
		szCLSID[i] = pwsz[i];	
	szCLSID[wcslen(pwsz)] = '\0';
	// get this DLL's path and file name
	GetModuleFileName(g_hInst, szModule, ARRAYSIZE(szModule));
	// Key's in register. MSDN
	REGSTRUCT ClsidEntries[] = {
		HKEY_CLASSES_ROOT, TEXT(".newTxtFile"),									NULL,                   TEXT("Shell.icon.karlodad"),
		HKEY_CLASSES_ROOT, TEXT("CLSID\\%s"),									NULL,                   TEXT("Shell.icon.karlodad"),
		HKEY_CLASSES_ROOT, TEXT("CLSID\\%s\\InprocServer32"),					NULL,                   TEXT("%s"),
		HKEY_CLASSES_ROOT, TEXT("CLSID\\%s\\InprocServer32"),					TEXT("ThreadingModel"), TEXT("Apartment"),
		HKEY_CLASSES_ROOT, TEXT("Shell.icon.karlodad"),							NULL,                   TEXT("The best icon 2019"),
		HKEY_CLASSES_ROOT, TEXT("Shell.icon.karlodad\\DefaultIcon"),			NULL,					TEXT("%%1"),
		HKEY_CLASSES_ROOT, TEXT("Shell.icon.karlodad\\Shellex\\IconHandler"),	NULL,					szCLSID,
		HKEY_CLASSES_ROOT, TEXT("Shell.icon.karlodad\\Shell"),					NULL,					TEXT("Run"),
		HKEY_CLASSES_ROOT, TEXT("Shell.icon.karlodad\\Shell\\Run"),				NULL,					TEXT("&��������� � ��������"),
		HKEY_CLASSES_ROOT, TEXT("Shell.icon.karlodad\\Shell\\Run\\command"),	NULL,					TEXT("notepad.exe %%1"),
		NULL,              NULL,												NULL,					NULL };
	for (i = 0; ClsidEntries[i].hRootKey; i++)
	{
		// Register all key's
		wsprintf(szSubKey, ClsidEntries[i].lpszSubKey, szCLSID);
		lResult = RegCreateKeyEx(ClsidEntries[i].hRootKey, szSubKey, 0,	NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hKey, &dwDisp);
		if (lResult == NOERROR)
		{
			TCHAR szData[MAX_PATH];
			wsprintf(szData, ClsidEntries[i].lpszData, szModule);
			lResult = RegSetValueEx(hKey, ClsidEntries[i].lpszValueName, 0,	REG_SZ,	(LPBYTE)szData,	lstrlen(szData) * 2 + 1);
			RegCloseKey(hKey);
		}
		else
			return SELFREG_E_CLASS;
	}
	// register the extension as approved by NT
	OSVERSIONINFO  osvi;
	osvi.dwOSVersionInfoSize = sizeof(osvi);
	GetVersionEx(&osvi);
	if (VER_PLATFORM_WIN32_NT == osvi.dwPlatformId)
	{
		lstrcpy(szSubKey, TEXT("Software\\Microsoft\\Windows\\CurrentVersion\\Shell Extensions\\Approved"));
		lResult = RegCreateKeyEx(HKEY_LOCAL_MACHINE, szSubKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE,	NULL, &hKey, &dwDisp);
		if (lResult == NOERROR)
		{
			TCHAR szData[MAX_PATH];
			lstrcpy(szData, TEXT("Shell.icon.karlodad"));
			lResult = RegSetValueEx(hKey, szCLSID, 0, REG_SZ, (LPBYTE)szData, lstrlen(szData) * 2 + 1);
			RegCloseKey(hKey);
		}
		else
			return SELFREG_E_CLASS;
	}
	SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_IDLIST, NULL, NULL);
	return S_OK;
}
/*---------------------------------------------------------------*/
// DllUnregisterServer()
/*---------------------------------------------------------------*/
STDAPI DllUnregisterServer(VOID)
{
	size_t i;
	LRESULT lResult;
	LPWSTR pwsz;
	StringFromIID(CLSID_SHELL, &pwsz);
	TCHAR szSubKey[MAX_PATH], szModule[MAX_PATH], *szCLSID = new TCHAR[wcslen(pwsz) + 1];
	for (i = 0; i < wcslen(pwsz); i++)	
		szCLSID[i] = pwsz[i];	
	szCLSID[wcslen(pwsz)] = '\0';
	// get this DLL's path and file name
	GetModuleFileName(g_hInst, szModule, ARRAYSIZE(szModule));
	REGSTRUCT ClsidEntries[] = {
		HKEY_CLASSES_ROOT, TEXT(".newTxtFile"),									NULL,                   NULL,
		HKEY_CLASSES_ROOT, TEXT("Shell.icon.karlodad\\Shell\\Run\\command"),	NULL,					NULL,
		HKEY_CLASSES_ROOT, TEXT("Shell.icon.karlodad\\Shell\\Run"),				NULL,					NULL,
		HKEY_CLASSES_ROOT, TEXT("Shell.icon.karlodad\\Shell"),					NULL,					NULL,
		HKEY_CLASSES_ROOT, TEXT("Shell.icon.karlodad\\Shellex\\IconHandler"),	NULL,					NULL,
		HKEY_CLASSES_ROOT, TEXT("Shell.icon.karlodad\\Shellex"),				NULL,					NULL,
		HKEY_CLASSES_ROOT, TEXT("Shell.icon.karlodad\\DefaultIcon"),			NULL,					NULL,
		HKEY_CLASSES_ROOT, TEXT("Shell.icon.karlodad"),							NULL,                   NULL,
		HKEY_CLASSES_ROOT, TEXT("CLSID\\%s\\InprocServer32\\ThreadingModel"),	NULL,					NULL,
		HKEY_CLASSES_ROOT, TEXT("CLSID\\%s\\InprocServer32"),					NULL,                   NULL,
		HKEY_CLASSES_ROOT, TEXT("CLSID\\%s"),									NULL,                   NULL,
		NULL,              NULL,												NULL,					NULL};
	for (i = 0; ClsidEntries[i].hRootKey; i++)
	{
		wsprintf(szSubKey, ClsidEntries[i].lpszSubKey, szCLSID);
		lResult = RegDeleteKey(ClsidEntries[i].hRootKey, szSubKey);
	}
	SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_IDLIST, NULL, NULL);
	return S_OK;
}