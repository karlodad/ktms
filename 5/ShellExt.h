#pragma once
#include "strsafe.h"
class CShellExt : public IPersistFile, IExtractIconW
{
	protected:
		DWORD m_ObjRefCount, m_dwMode, m_qwFileSize;
		TCHAR m_szFileName[MAX_PATH];    // The file name
	public:
		CShellExt();
		~CShellExt();
		// IUnknown methods
		STDMETHOD(QueryInterface) (REFIID riid, LPVOID * ppvObj);
		STDMETHOD_(ULONG, AddRef) (void);
		STDMETHOD_(ULONG, Release) (void);
		// IPersistFile
		STDMETHODIMP GetClassID(CLSID* pCLSID);
		STDMETHODIMP IsDirty() { return E_NOTIMPL; }
		STDMETHODIMP Save(LPCOLESTR, BOOL) { return E_NOTIMPL; }
		STDMETHODIMP SaveCompleted(LPCOLESTR) { return E_NOTIMPL; }
		STDMETHODIMP GetCurFile(LPOLESTR*) { return E_NOTIMPL; }
		STDMETHODIMP Load(PCWSTR pszFile, DWORD dwMode){
			m_dwMode = dwMode;
			return StringCchCopy(m_szFileName, ARRAYSIZE(m_szFileName), pszFile);
		}
		// IExtractIcon
		STDMETHODIMP GetIconLocation(UINT uFlags, LPTSTR szIconFile, UINT cchMax, int* piIndex, UINT* pwFlags);
		STDMETHODIMP Extract(LPCTSTR pszFile, UINT nIconIndex, HICON* phiconLarge, HICON* phiconSmall, UINT nIconSize) {
			return S_FALSE;
		}
};

