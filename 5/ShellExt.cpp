#include <windows.h>
#include <tchar.h>
#include <shlobj.h>
#include <comutil.h>
#include "ShellExt.h"
#include "Guid.h"
#include "ClassFactory.h"
#pragma comment( lib, "comsuppwd.lib")

CShellExt::CShellExt() {}
CShellExt::~CShellExt() {}
/*---------------------------------------------------------------*/
// Release()
/*---------------------------------------------------------------*/
STDMETHODIMP_(DWORD) CShellExt::Release(void)
{
	if (--m_ObjRefCount == 0)
	{
		delete this;
		return 0;
	}
	return m_ObjRefCount;
}
/*---------------------------------------------------------------*/
// AddRef()
/*---------------------------------------------------------------*/
STDMETHODIMP_(DWORD) CShellExt::AddRef(void)
{
	return ++m_ObjRefCount;
}
/*---------------------------------------------------------------*/
// QueryInterface()
/*---------------------------------------------------------------*/
STDMETHODIMP CShellExt::QueryInterface(REFIID riid, LPVOID *ppReturn)
{
	*ppReturn = NULL;
	if (IsEqualIID(riid, IID_IUnknown))
		*ppReturn = this;
	else if (IsEqualIID(riid, IID_IClassFactory))
		*ppReturn = (IClassFactory*)this;
	else if (IsEqualIID(riid, IID_IPersistFile))
		*ppReturn = (IPersistFile*)this;
	else if (IsEqualIID(riid, IID_IExtractIconW))
		*ppReturn = (IExtractIconW*)this;
	//IPersistFile, IExtractIcon
	if (*ppReturn)
	{
		LPUNKNOWN pUnk = (LPUNKNOWN)(*ppReturn);
		pUnk->AddRef();
		return S_OK;
	}
	return E_NOINTERFACE;
}
/*---------------------------------------------------------------*/
 // GetIconLocation()
 /*---------------------------------------------------------------*/
STDMETHODIMP CShellExt::GetIconLocation(UINT uFlags, LPTSTR szIconFile, UINT cchMax, int  *piIndex, UINT *pwFlags)
{
	DWORD     dwFileSizelow, dwFileSizeHign, number, countLine = 0;
	DWORDLONG qwSize;
	HANDLE    hFile;	
	hFile = CreateFile(m_szFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (INVALID_HANDLE_VALUE == hFile)
		return S_FALSE;

	dwFileSizelow = GetFileSize(hFile, &dwFileSizeHign);
	if ((DWORD)-1 == dwFileSizelow && GetLastError() != NO_ERROR)
		return S_FALSE;

	qwSize = DWORDLONG(dwFileSizeHign) << 32 | dwFileSizelow; 
	char *mas = new char[qwSize];
	ReadFile(hFile, mas, qwSize, &number, NULL);
	CloseHandle(hFile); 
	for (int i = 0; i < qwSize; i++)
		if (mas[i] == '\n')
			countLine++;
	delete []mas;
	if (qwSize == 0)
		*piIndex = 0; // use icon with index 0
	else if (countLine + 1 < 100)
		*piIndex = 1; // use icon with index 1
	else if (countLine + 1 < 500)
		*piIndex = 2; // use icon with index 2
	else
		*piIndex = 3; // use icon with index 3
	TCHAR szModulePath[MAX_PATH];
	GetModuleFileName(g_hInst, szModulePath, MAX_PATH);
	lstrcpyn(szIconFile, szModulePath, cchMax);
	*pwFlags = 0;
	return S_OK;
}
/*---------------------------------------------------------------*/
 // GetClassID()
 /*---------------------------------------------------------------*/
STDMETHODIMP CShellExt::GetClassID(CLSID* pCLSID)
{
	*pCLSID = CLSID_SHELL;
	return S_OK;
}