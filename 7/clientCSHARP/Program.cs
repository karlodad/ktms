﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Type ShellType = Type.GetTypeFromProgID("Transfer.component");
                Object ShellObj = Activator.CreateInstance(ShellType);
                Object obj = ShellType.InvokeMember("KMtoM", BindingFlags.InvokeMethod, null, ShellObj, new object[1] { 20.0 });
                Console.WriteLine("{0}: {1} = {2}", "KMtoM", 20.0, obj.ToString());
            }
            catch(Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
            }
            Console.ReadKey();
        }
    }
}
