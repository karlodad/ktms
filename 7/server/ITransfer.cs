﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Server
{
    [Guid("D06BFEE0-77F1-49E4-8C4E-2046270FE100")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface ITransfer
    {
        [DispId(1)]
        double MtoKM(double a);

        [DispId(2)]
        double MtoCM(double a);

        [DispId(3)]
        double CMtoKM(double a);

        [DispId(4)]
        double CMtoM(double a);

        [DispId(5)]
        double KMtoCM(double a);

        [DispId(6)]
        double KMtoM(double a);
    }
}
