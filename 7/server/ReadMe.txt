1. Create interface ITransfer with annotation:
    	* [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)] for sure
	* [Guid("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")]
    /add methods
2. Create interface for com Interactions with:
    	* [Guid("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ")]
    	* [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
   /not create method's
3. Create class implementations ITransfer with:
    	* [Guid("YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY")]
    	* [ClassInterface(ClassInterfaceType.None)] for sure
    	* [ComSourceInterfaces(typeof(IComEvent))] for sure
    	* [ProgId("Transfer.component")]
   /implement methods
4. Sign assembly in parameters
5. In AssemblyInfo.cs change:
[assembly: ComVisible(false)] -> [assembly: ComVisible(true)]
6. Compile this project as dll (I compiled all with .Net Framework 4.0)
7. Register dll:
	"C:\Windows\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe" server.dll