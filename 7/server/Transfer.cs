﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Server
{
    [Guid("14279374-FA1A-44EA-946F-21D4356B6F99")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComSourceInterfaces(typeof(IComEvent))]
    [ProgId("Transfer.component")]
    public class Transfer : ITransfer
    {
        public Transfer() { }
        public double CMtoKM(double a)
        {
            return a / 100000;
        }

        public double CMtoM(double a)
        {
            return a / 100;
        }

        public double KMtoCM(double a)
        {
            return a * 100000;
        }

        public double KMtoM(double a)
        {
            return a * 1000;
        }

        public double MtoCM(double a)
        {
            return a * 100;
        }

        public double MtoKM(double a)
        {
            return a / 1000;
        }
    }
}
