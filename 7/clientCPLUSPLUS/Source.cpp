#include <windows.h> 
#include <tchar.h> 
#include <iostream> 
#include <initguid.h> 
#include "itransfer.h"

using namespace std;

int main(int argc, char *argv[])
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cout << "������������� ���������� COM " << endl;
	if (FAILED(CoInitialize(NULL)))
	{
		cout << "������. ������ ���������������� COM " << endl;
		return -1;
	}
	char* szProgID = "Transfer.Component";
	WCHAR szWideProgID[128];
	CLSID clsid;
	long lLen = MultiByteToWideChar(CP_ACP, 0, szProgID, strlen(szProgID), szWideProgID, sizeof(szWideProgID));
	szWideProgID[lLen] = '\0';
	HRESULT hr = ::CLSIDFromProgID(szWideProgID, &clsid);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "������ �������� CLSID ��� ProgID . HR = " << hr << endl;
		return -1;
	}
	IClassFactory* pCF;
	hr = CoGetClassObject(clsid, CLSCTX_INPROC, NULL, IID_IClassFactory, (void**)&pCF);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "�� ������� ������� ������ �������. HR = " << hr << endl;
		return -1;
	}
	IUnknown* pUnk;
	hr = pCF->CreateInstance(NULL, IID_IUnknown, (void**)&pUnk);
	pCF->Release();
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "�� ������� ������� �������� �������. HR = " << hr << endl;
		return -1;
	}
	cout << "��������� ������� ������" << endl;
	ITransfer* pTrans = NULL;
	hr = pUnk->QueryInterface(IID_Transfer, (LPVOID*)&pTrans);
	pUnk->Release();
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "QueryInterface() ��� ITransfer �� ��������. HR = " << hr << endl;
		return -1;
	}
	double result;
	pTrans->MtoKM(1500.0, &result);
	cout << "15000MtoKM=" << result << endl;
	pTrans->KMtoCM(1.0, &result);
	cout << "1KMtoCM=" << result << endl;
	pTrans->KMtoM(5, &result);
	cout << "5KMtoM=" << result << endl;
	cout << "����������� ���������" << endl;
	pTrans->Release();
	cout << "���������� ���������� COM " << endl;
	CoUninitialize();
	return 0;
}