#pragma once
// [Guid("D06BFEE0-77F1-49E4-8C4E-2046270FE100")]
DEFINE_GUID(IID_Transfer, 0xD06BFEE0, 0x77F1, 0x49E4, 0x8C, 0x4E, 0x20, 0x46, 0x27, 0x0F, 0xE1, 0x00);

class ITransfer : public IUnknown
{
public:
	STDMETHOD(MtoKM(double, double *))PURE;
	STDMETHOD(MtoCM(double, double *))PURE;
	STDMETHOD(CMtoKM(double, double *))PURE;
	STDMETHOD(CMtoM(double, double *))PURE;
	STDMETHOD(KMtoCM(double, double *))PURE;
	STDMETHOD(KMtoM(double, double *))PURE;
};