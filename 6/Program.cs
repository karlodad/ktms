﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using shells;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> map = new Dictionary<string, string>();
            map.Add("1", "MtoKM"); map.Add("2", "MtoCM");
            map.Add("3", "CMtoKM"); map.Add("4", "CMtoM");
            map.Add("5", "KMtoCM"); map.Add("6", "KMtoM");
            try
            {
                String choise;
                Double temp;
                Console.WriteLine("Режим работы:");
                Console.WriteLine("1. Reflection");
                Console.WriteLine("2. Сборка переходник");
                Console.Write(">");
                choise = Console.ReadLine();
                Console.WriteLine("=====================================================");
                if (choise.Equals("1"))
                {
                    Type ShellType = Type.GetTypeFromProgID("Transfer.component");
                    Object ShellObj = Activator.CreateInstance(ShellType);
                    Console.WriteLine("Выбор метода:");
                    foreach (string key in map.Keys)
                        Console.WriteLine("{0}. {1}", key, map[key]);
                    Console.Write(">");
                    while (!map.ContainsKey(choise = Console.ReadLine())) ;
                    Console.WriteLine("Параметр:");
                    Console.Write(">");
                    while (!Double.TryParse(Console.ReadLine(), out temp)) ;
                    Object obj = ShellType.InvokeMember(map[choise], BindingFlags.InvokeMethod, null, ShellObj, new object[1] { temp });
                    Console.WriteLine("{0}: {1} = {2}", map[choise], temp, obj.ToString());
                }
                else
                {
                    Transfer.ITransfer it = new Transfer.Transfer();
                    Console.WriteLine("Параметр:");
                    Console.Write(">");
                    while (!Double.TryParse(Console.ReadLine(), out temp)) ;
                    Console.WriteLine("{0}: {1} = {2}", map["1"], temp, it.MtoKM(temp));
                    Console.WriteLine("{0}: {1} = {2}", map["2"], temp, it.MtoCM(temp));
                    Console.WriteLine("{0}: {1} = {2}", map["3"], temp, it.CMtoKM(temp));
                    Console.WriteLine("{0}: {1} = {2}", map["4"], temp, it.MtoKM(temp));
                    Console.WriteLine("{0}: {1} = {2}", map["5"], temp, it.CMtoM(temp));
                    Console.WriteLine("{0}: {1} = {2}", map["6"], temp, it.KMtoM(temp));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
            }
            Console.WriteLine("=====================================================");
            Console.ReadKey();
        }
    }
}
