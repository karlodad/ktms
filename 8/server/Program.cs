﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                HttpChannel chan = new HttpChannel(65100);
                ChannelServices.RegisterChannel(chan);
                Type typeCalc = Type.GetType("Server.Calculator");
                Type typeTrans = Type.GetType("Server.Transfer");
                RemotingConfiguration.RegisterWellKnownServiceType(typeCalc, "theEndPoint", WellKnownObjectMode.SingleCall);
                RemotingConfiguration.RegisterWellKnownServiceType(typeTrans, "transfer", WellKnownObjectMode.Singleton);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error:{0}", e.Message);
            }
            Console.ReadKey();
        }
    }
}
