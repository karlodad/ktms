﻿using InterfaceCalc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Calculator : MarshalByRefObject, ICalc
    {
        public Calculator()
        {
            Console.WriteLine("Я родился калькулятором");
        }
        public double add(double a, double b)
        {
            Console.WriteLine("{0}+{1}={2}", a, b, a + b);
            return a + b;
        }

        public double sub(double a, double b)
        {
            Console.WriteLine("{0}-{1}={2}", a, b, a - b);
            return a - b;
        }
    }
}
