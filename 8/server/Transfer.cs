﻿using IntrefaceTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Transfer : MarshalByRefObject, ITransfer
    {
        public Transfer()
        {
            Console.WriteLine("Я родился трансформатором");
        }
        public double CMtoKM(double a)
        {
            Console.WriteLine("CMtoKM {0}", a);
            return a / 100000;
        }

        public double CMtoM(double a)
        {
            Console.WriteLine("CMtoM {0}", a);
            return a / 100;
        }

        public double KMtoCM(double a)
        {
            Console.WriteLine("KMtoCM {0}", a);
            return a * 100000;
        }

        public double KMtoM(double a)
        {
            Console.WriteLine("KMtoM {0}", a);
            return a * 1000;
        }

        public double MtoCM(double a)
        {
            Console.WriteLine("MtoCM {0}", a);
            return a * 100;
        }

        public double MtoKM(double a)
        {
            Console.WriteLine("MtoKM {0}", a);
            return a / 1000;
        }
    }
}
