﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    using InterfaceCalc;
    using IntrefaceTransfer;
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                HttpChannel chan = new HttpChannel(0);
                ChannelServices.RegisterChannel(chan);
                MarshalByRefObject objCalc = (MarshalByRefObject)RemotingServices.Connect(typeof(ICalc), "http://localhost:65100/theEndPoint");
                MarshalByRefObject objTransfer = (MarshalByRefObject)RemotingServices.Connect(typeof(ITransfer), "http://localhost:65100/transfer");
                ICalc calc = objCalc as ICalc;
                ITransfer transfer = objTransfer as ITransfer;
                Console.WriteLine("3+4 = {0}", calc.add(3.0, 5.0));
                Console.WriteLine("3-5 = {0}", calc.sub(3.0, 5.0));

                Console.WriteLine("KMtoM 222 = {0}", transfer.KMtoM(222));
                Console.WriteLine("MtoKM 12354 = {0}", transfer.MtoKM(12354));
            }
            catch (Exception e)
            {
                Console.WriteLine("Error:{0}", e.Message);
            }
            Console.ReadKey();
        }
    }
}
