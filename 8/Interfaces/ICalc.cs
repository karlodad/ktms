﻿namespace InterfaceCalc
{
    using System;
    public interface ICalc
    {
        double add(double a, double b);
        double sub(double a, double b);
    }
}
