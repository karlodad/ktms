﻿namespace IntrefaceTransfer
{
    using System;
    public interface ITransfer
    {
        double MtoKM(double a);
        double MtoCM(double a);
        double CMtoKM(double a);
        double CMtoM(double a);
        double KMtoCM(double a);
        double KMtoM(double a);
    }
}
